﻿create table employee (
	id integer not null primary key,
	name text,
	salary integer,
	manager_id integer,
	department_id integer
	)

insert into employee(id, name, salary) values (1, 'Daddy', 10), (2, 'Mark', 20),
	(3,'Rut', 17), (4, 'Lue', 9), (5, 'Pen', 28)

select salary from employee where name='Rut'

update employee set salary=11 where name='Daddy'

delete from employee where name='Lue'

alter table employee add column kkk(idu integer)

insert into employee(id, name, salary, department_id) values (4, 'Rue', 34, 1), (6, 'Dark', 18, 1)

update employee set department_id=2 where id in(1, 3)

update employee set department_id=3 where id in(2, 5)

select * from employee

select MAX(salary), department_id from employee group by department_id

select avg(salary), department_id from employee group by department_id having avg(salary)>10

alter table employee add constraint manager_employee foreign key(manager_id) references employee(id)

update employee set manager_id=6 where id in (1, 3)

update employee set manager_id=5 where id in (4, 2)

select emp.name, emp.salary, employee.salary from employee as emp 
join employee on emp.manager_id= employee.id where emp.salary>employee.salary

select name, salary, department_id from employee as emp where salary >= ( 
	select max(salary) from employee  where emp.department_id=employee.department_id)





