﻿create table main_table (
	cluster_id text not null);

create table added (
	cluster_id text not null,
	date_added date not null);

	
create table deleted (
	cluster_id text not null);


alter table deleted add column date_delete date;

select * from deleted;


//script


with deleted_temp as (select * from added where added.cluster_id not in (select main_table.cluster_id from main_table))

insert into deleted select cluster_id, current_date from deleted_temp;

with deleted_temp as (select * from added where added.cluster_id not in (select main_table.cluster_id from main_table))

delete from added where added.cluster_id in (select cluster_id from deleted_temp);

with added_temp as (select * from main_table where main_table.cluster_id not in (select added.cluster_id from added))

delete from deleted where cluster_id in (select cluster_id from added_temp);

with added_temp as (select * from main_table where main_table.cluster_id not in (select added.cluster_id from added))

insert into added select cluster_id, current_date from added_temp;


select cluster_id from added where date_added >= current_date-10;

select cluster_id from deleted order by date_delete ASC limit 10;



//insert test data

insert into added values(3, '2017-10-10');
insert into added values(5, '2017-09-10');
insert into deleted values(2, '2017-08-10');
insert into deleted values(4, '2017-09-09');
insert into deleted values(8, '2017-08-10');
insert into deleted values(9, '2017-09-09');
insert into deleted values(10, '2017-08-10');
insert into deleted values(11, '2017-09-09');
insert into deleted values(12, '2017-08-10');
insert into deleted values(13, '2017-09-09');
insert into deleted values(14, '2017-08-10');
insert into deleted values(15, '2017-09-09');
insert into deleted values(16, '2017-08-10');
insert into deleted values(17, '2017-09-09');

update deleted set date_delete='2017-07-07' where cluster_id='8';

select * from added;
select * from deleted;
select * from main_table;

delete from added;
delete from deleted;

insert into main_table values(3);
insert into main_table values(2);
insert into main_table values(7);


