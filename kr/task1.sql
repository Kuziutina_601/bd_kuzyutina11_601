﻿create table type_color (
	id serial,
	"type" text not null,
	"color" text not null);


insert into type_color ("type", color) values ('dog','black');
insert into type_color ("type", color) values ('cat','gray');
insert into type_color ("type", color) values ('dog','white');
insert into type_color ("type", color) values ('fox','red');
insert into type_color ("type", color) values ('dog','black');

select * from type_color;

alter table type_color add column counter integer;

with count_all as (select "type", color, count(*) from type_color group by "type", color)

update type_color set counter = (select count_all.count from count_all where (type_color.type = count_all.type) and (type_color.color = count_all.color));

delete from type_color where id not in(select min(id) from type_color group by (type, color));

