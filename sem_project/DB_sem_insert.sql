﻿INSERT INTO public.friends (user_id, friend_id) VALUES (18, 19);
INSERT INTO public.friends (user_id, friend_id) VALUES (18, 20);
INSERT INTO public.friends (user_id, friend_id) VALUES (18, 24);


INSERT INTO public.letters (id, header, body, date, sender_id, recepient_id) VALUES (17, '', 'Здравствуй, Дмитрий, как твои успехи??', '2017-11-21 02:37:01.048000', 18, 20);
INSERT INTO public.letters (id, header, body, date, sender_id, recepient_id) VALUES (18, 'Hi', 'Почему молчишь?????', '2017-11-21 02:39:53.888000', 18, 20);
INSERT INTO public.letters (id, header, body, date, sender_id, recepient_id) VALUES (19, 'Занят', 'Я пока занят, к сожалению, ни на что не хватает времени ', '2017-11-21 02:43:58.654000', 20, 18);
INSERT INTO public.letters (id, header, body, date, sender_id, recepient_id) VALUES (20, '', 'gbkjbjhj', '2017-11-21 10:11:44.694000', 18, 19);
INSERT INTO public.letters (id, header, body, date, sender_id, recepient_id) VALUES (21, 'тема', 'сообщение', '2017-11-21 10:33:06.837000', 18, 19);
INSERT INTO public.letters (id, header, body, date, sender_id, recepient_id) VALUES (22, '', 'ds', '2017-11-22 01:09:36.340000', 18, 24);



INSERT INTO public.magazines (id, name, description, picture_path, path) VALUES (37, 'Сабрина', 'Нам не страшны ни морозы, ни метели, ни долгие темные вечера. Для людей, у которых есть такое захватывающее хобби, как вязание, непогода – верный союзник, ведь появляется так много времени для творчества. И не только... Вы можете еще и заботиться о своем здоровье. Признано, что вязание оказывает стимулирующее воздействие на мозговую деятельность, снижает кровяное давление, гармонизует работу сердечно-сосудистой системы. Вооружитесь терпением, нашими советами, хорошим настроением и свяжите потрясающие теплые наряды для зимы. Вы можете создать собственными руками очаровательные обновки и подарки всем, всем, всем – и не только на Рождество. Желаем вам вдохновения в творчестве!', 'project_images/bd3900a1-3da8-486b-bcfc-b73263bc9414.jpeg', NULL);
INSERT INTO public.magazines (id, name, description, picture_path, path) VALUES (38, 'Forbes', 'американский финансово-экономический журнал, одно из наиболее авторитетных и известных экономических печатных изданий в мире. Основан в 1917 году Берти Чарлзом Форбсом.', 'project_images/1239f8e9-c023-4a97-87ad-491ba6bc53aa.jpeg', NULL);
INSERT INTO public.magazines (id, name, description, picture_path, path) VALUES (39, 'Psychology', 'журнал это', 'project_images/d3dea622-d42b-4a2f-971a-1227edefb5f5.jpeg', NULL);
INSERT INTO public.magazines (id, name, description, picture_path, path) VALUES (40, 'Вязание крючком', 'Вязание шаг за шагом', 'project_images/2ca45380-1d53-4736-ac7e-b0c16112cd2b.jpeg', NULL);


INSERT INTO public.magazines_copies (id, name, description, picture_path, path, date, magazine_id) VALUES (7, 'Выпуск 1', 'Нам не страшны ни морозы, ни метели, ни долгие темные вечера. Для людей, у которых есть такое захватывающее хобби, как вязание, непогода – верный союзник, ведь появляется так много времени для творчества. И не только... Вы можете еще и заботиться о своем здоровье. Признано, что вязание оказывает стимулирующее воздействие на мозговую деятельность, снижает кровяное давление, гармонизует работу сердечно-сосудистой системы. Вооружитесь терпением, нашими советами, хорошим настроением и свяжите потрясающие теплые наряды для зимы. Вы можете создать собственными руками очаровательные обновки и подарки всем, всем, всем – и не только на Рождество. Желаем вам вдохновения в творчестве!', 'project_images/adf1d018-fca5-4159-bbc9-a4d0e020457d.jpeg', 'project_documents/a866c2dd-0228-4d96-ae0a-dbcf6079d3c8..pdf', '2017-11-21 01:53:09.059000', 37);
INSERT INTO public.magazines_copies (id, name, description, picture_path, path, date, magazine_id) VALUES (8, 'Выпуск 2', 'Прекрасные вязаные модели для жарких летних дней для опытных и начинающих мастериц.', 'project_images/91583864-3f39-48f1-b87e-ddc709033a33.jpeg', 'project_documents/915b18c2-2e7e-4412-8c64-5b90a5d3b232..pdf', '2017-11-21 01:54:42.258000', 37);
INSERT INTO public.magazines_copies (id, name, description, picture_path, path, date, magazine_id) VALUES (9, 'Выпуск 3', '', 'project_images/cb1a303e-2d2f-4bcb-abac-9707f204f54d.jpeg', 'project_documents/8e37f424-3b0c-4437-8c5f-16b5ef8fdc24..pdf', '2017-11-21 01:55:31.302000', 37);
INSERT INTO public.magazines_copies (id, name, description, picture_path, path, date, magazine_id) VALUES (10, 'Выпуск 4', '', 'project_images/46d21bb2-d928-4843-813d-80a0194c4cb8.jpeg', 'project_documents/c15c5235-6696-456b-a4f9-b281b4fc1891..pdf', '2017-11-21 01:56:10.994000', 37);
INSERT INTO public.magazines_copies (id, name, description, picture_path, path, date, magazine_id) VALUES (11, 'Выпуск 1', '', 'project_images/4b5826fc-5184-4656-a9e9-1729d9513242.jpeg', 'project_documents/76a9305c-9077-4d4d-9418-4ac584dde923..pdf', '2017-11-21 01:58:23.316000', 38);
INSERT INTO public.magazines_copies (id, name, description, picture_path, path, date, magazine_id) VALUES (12, 'Выпуск 1', '', 'project_images/add32edd-4df9-497c-8cf6-193ebbbdc900.jpeg', 'project_documents/59df1258-5150-4a9e-a4ba-4767638ebab3..pdf', '2017-11-21 01:59:47.236000', 39);
INSERT INTO public.magazines_copies (id, name, description, picture_path, path, date, magazine_id) VALUES (13, 'Выпуск 2', '', 'project_images/e0dc304e-9ab6-4929-b5fc-fa32f0a86b46.jpeg', 'project_documents/c514d178-903b-4658-b51e-59654f66099c..pdf', '2017-11-21 02:00:27.199000', 39);
INSERT INTO public.magazines_copies (id, name, description, picture_path, path, date, magazine_id) VALUES (14, 'Выпуск 1', '', 'project_images/fdc6962d-cd2c-4b71-b796-75c6bdc57a0c.jpeg', 'project_documents/afd8c109-03eb-4ec3-8302-3c62cc74147e..pdf', '2017-11-21 02:01:17.777000', 40);
INSERT INTO public.magazines_copies (id, name, description, picture_path, path, date, magazine_id) VALUES (15, 'Выпуск 2', '', 'project_images/4fc75579-1cd8-4fdc-a191-e51a5b0ce7c9.jpeg', 'project_documents/889a146c-c7e0-42f2-8424-5672d303625a..pdf', '2017-11-21 02:01:33.333000', 40);

INSERT INTO public.subscriptions (user_id, magazine_id) VALUES (18, 40);
INSERT INTO public.subscriptions (user_id, magazine_id) VALUES (18, 37);
INSERT INTO public.subscriptions (user_id, magazine_id) VALUES (18, 38);
INSERT INTO public.subscriptions (user_id, magazine_id) VALUES (18, 39);


INSERT INTO public.users (id, login, password, name, cookie, confirmation) VALUES (23, '2@yandex.ru', '79b7cdcd', 'Ольга', NULL, 'd790719e-b23b-466a-ad27-764a4843ca19');
INSERT INTO public.users (id, login, password, name, cookie, confirmation) VALUES (21, '5@yandex.ru', '79b7cdc', 'Михаил', NULL, '64e179ff-2be0-4354-b86d-a114e5dfe35e');
INSERT INTO public.users (id, login, password, name, cookie, confirmation) VALUES (19, '3@yandex.ru', '8', 'Екатерина', NULL, '5a2ff460-648f-45f7-b5c5-96f17ee002e9');
INSERT INTO public.users (id, login, password, name, cookie, confirmation) VALUES (22, '1@yandex.ru', '827ccbeea8a7', 'Вячеслав', NULL, '');
INSERT INTO public.users (id, login, password, name, cookie, confirmation) VALUES (20, '4@yandex.ru', 'b7bc2', 'Дмитрий', NULL, 'd4b7fa4c-866b-4554-a948-631f7de6d990');
INSERT INTO public.users (id, login, password, name, cookie, confirmation) VALUES (24, 'c@mail.ru', 'd85', 'Наталья', NULL, '8a5239f7-c39a-4887-95a6-23c04358cabc');
INSERT INTO public.users (id, login, password, name, cookie, confirmation) VALUES (18, 'cuzya@yandex.ru', 'd8578', 'Софья', '', 'e9461464-1f74-439f-ad9c-91e20015d13b');
INSERT INTO public.users (id, login, password, name, cookie, confirmation) VALUES (25, 'kkk@yandex.ru', '827c', 'Соня', NULL, '4c75e59b-3342-46cf-ab09-6205a17a49c7');

