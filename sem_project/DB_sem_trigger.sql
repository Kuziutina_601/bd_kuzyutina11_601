﻿CREATE TABLE "magazines_log" (
"doing" TEXT,
"date" TIMESTAMP NOT NULL,
"id"  INTEGER NOT NULL ,
"name" VARCHAR(30) ,
"description" TEXT ,
"picture_path" VARCHAR(60) ,
"path" VARCHAR(60)
);

CREATE OR REPLACE FUNCTION update_magazine_logs() RETURNS TRIGGER AS $$
    BEGIN
        IF (TG_OP = 'DELETE') THEN
            INSERT INTO magazines_log VALUES('DELETE', now(), OLD.*);
            RETURN OLD;
        ELSIF (TG_OP = 'UPDATE') THEN
            INSERT INTO magazines_log VALUES('UPDATE', now(), NEW.*);
            RETURN NEW;
        ELSIF (TG_OP = 'INSERT') THEN
            INSERT INTO magazines_log VALUES('INSERT', now(), NEW.*);
            RETURN NEW;
        END IF;
    END;
$$ LANGUAGE plpgsql;


create trigger update_magazine_logs before update or insert or delete on "magazines"
	for each row
	execute procedure update_magazine_logs();
	





CREATE TABLE "magazines_copies_log" (
"doing" TEXT,
"date_d" TIMESTAMP NOT NULL,
"id"  INTEGER NOT NULL ,
"name" VARCHAR(30) ,
"description" TEXT ,
"picture_path" VARCHAR(60) ,
"path" VARCHAR(60) ,
"date" TIMESTAMP ,
"magazine_id" INTEGER
);


CREATE OR REPLACE FUNCTION update_magazine_copy_logs() RETURNS TRIGGER AS $$
    BEGIN
        IF (TG_OP = 'DELETE') THEN
            INSERT INTO magazines_copies_log VALUES('DELETE', now(), OLD.*);
            RETURN OLD;
        ELSIF (TG_OP = 'UPDATE') THEN
            INSERT INTO magazines_copies_log VALUES('UPDATE', now(), NEW.*);
            RETURN NEW;
        ELSIF (TG_OP = 'INSERT') THEN
            INSERT INTO magazines_copies_log VALUES('INSERT', now(), NEW.*);
            RETURN NEW;
        END IF;
    END;
$$ LANGUAGE plpgsql;


create trigger update_magazine_copy_logs before update or insert or delete on "magazines_copies"
	for each row
	execute procedure update_magazine_copy_logs();





CREATE TABLE "users_log" (
"doing" TEXT,
"date_d" TIMESTAMP NOT NULL,
"id"  INTEGER ,
"login" VARCHAR(20) NOT NULL ,
"password" VARCHAR(20) NOT NULL ,
"name" VARCHAR(20) ,
"confirmation" TEXT ,
"cookie" TEXT
);

CREATE OR REPLACE FUNCTION update_user_logs() RETURNS TRIGGER AS $$
    BEGIN
        IF (TG_OP = 'DELETE') THEN
            INSERT INTO users_log VALUES('DELETE', now(), OLD.*);
            RETURN OLD;
        ELSIF (TG_OP = 'UPDATE') THEN
            INSERT INTO users_log VALUES('UPDATE', now(), NEW.*);
            RETURN NEW;
        ELSIF (TG_OP = 'INSERT') THEN
            INSERT INTO users_log VALUES('INSERT', now(), NEW.*);
            RETURN NEW;
        END IF;
    END;
$$ LANGUAGE plpgsql;


create trigger update_user_logs before update or insert or delete on "users"
	for each row
	execute procedure update_user_logs();
	
	
