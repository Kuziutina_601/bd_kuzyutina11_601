﻿CREATE TABLE "magazines" (
"id"  SERIAL NOT NULL ,
"name" VARCHAR(30) ,
"description" TEXT ,
"picture_path" VARCHAR(60) ,
"path" VARCHAR(60) ,
PRIMARY KEY ("id")
);

CREATE TABLE "magazines_copies" (
"id"  SERIAL ,
"name" VARCHAR(30) ,
"description" TEXT ,
"picture_path" VARCHAR(60) ,
"path" VARCHAR(60) ,
"date" TIMESTAMP ,
"magazine_id" INTEGER ,
PRIMARY KEY ("id")
);

CREATE TABLE "quotations" (
"quotation" TEXT NOT NULL DEFAULT 'NULL' ,
"magazine_id" INTEGER NOT NULL ,
"author" TEXT NOT NULL DEFAULT 'NULL' 
);

ALTER TABLE "quotations" ADD FOREIGN KEY ("magazine_id") REFERENCES "magazines" ("id");

CREATE TABLE "users" (
"id"  SERIAL NOT NULL ,
"login" VARCHAR(20) NOT NULL ,
"password" VARCHAR(20) NOT NULL ,
"name" VARCHAR(20) ,
"confirmation" TEXT ,
"cookie" TEXT ,
PRIMARY KEY ("id")
);

CREATE TABLE "friends" (
"user_id"  SERIAL ,
"friend_id" INTEGER 
);

CREATE TABLE "letters" (
"id"  SERIAL NOT NULL ,
"header" VARCHAR(30) ,
"body" TEXT ,
"date" TIMESTAMP ,
"sender_id" INTEGER ,
"recepient_id" INTEGER ,
PRIMARY KEY ("id")
);

CREATE TABLE "subscriptions" (
"user_id" INTEGER ,
"magazine_id" INTEGER
);

CREATE TABLE "reviews" (
"id"  SERIAL ,
"review" TEXT ,
"date" TIMESTAMP NOT NULL ,
"score" SMALLINT NOT NULL ,
"user_id" INTEGER NOT NULL ,
PRIMARY KEY ("id")
);

CREATE TABLE "magazines_reviews" (
"magazine_id" INTEGER ,
"review_id" INTEGER
);

CREATE TABLE "magazines_copies_reviews" (
"magazine_copy_id" INTEGER NOT NULL ,
"review_id" INTEGER NOT NULL
);

ALTER TABLE "magazines_copies" ADD FOREIGN KEY ("magazine_id") REFERENCES "magazines" ("id");
ALTER TABLE "friends" ADD FOREIGN KEY ("user_id") REFERENCES "users" ("id");
ALTER TABLE "friends" ADD FOREIGN KEY ("friend_id") REFERENCES "users" ("id");
ALTER TABLE "letters" ADD FOREIGN KEY ("sender_id") REFERENCES "users" ("id");
ALTER TABLE "letters" ADD FOREIGN KEY ("recepient_id") REFERENCES "users" ("id");
ALTER TABLE "subscriptions" ADD FOREIGN KEY ("user_id") REFERENCES "users" ("id");
ALTER TABLE "subscriptions" ADD FOREIGN KEY ("magazine_id") REFERENCES "magazines" ("id");
ALTER TABLE "reviews" ADD FOREIGN KEY ("user_id") REFERENCES "users" ("id");
ALTER TABLE "magazines_reviews" ADD FOREIGN KEY ("magazine_id") REFERENCES "magazines" ("id");
ALTER TABLE "magazines_reviews" ADD FOREIGN KEY ("review_id") REFERENCES "reviews" ("id");
ALTER TABLE "magazines_copies_reviews" ADD FOREIGN KEY ("magazine_copy_id") REFERENCES "magazines_copies" ("id");
ALTER TABLE "magazines_copies_reviews" ADD FOREIGN KEY ("review_id") REFERENCES "reviews" ("id");