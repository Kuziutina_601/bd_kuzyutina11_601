﻿CREATE TABLE "magazines_magazines_copies" (
"magazine_id" INTEGER NOT NULL ,
"name" VARCHAR(30) ,
"description" TEXT ,
"picture_path" VARCHAR(60) ,
"path" VARCHAR(60) ,
"magazine_copy_id" INTEGER NOT NULL,
"name_copy" VARCHAR(30) ,
"description_copy" TEXT ,
"picture_path_copy" VARCHAR(60) ,
"path_copy" VARCHAR(60) ,
"date_copy" TIMESTAMP
);



CREATE OR REPLACE FUNCTION public.update_magazine_magazine_copy()
  RETURNS trigger AS
    BEGIN
        IF (TG_OP = 'DELETE') THEN
            DELETE FROM magazines_magazines_copies WHERE  magazine_id = OLD.magazine_id and magazine_copy_id = OLD.id;
            RETURN OLD;
        ELSIF (TG_OP = 'UPDATE') THEN
	    UPDATE magazines_magazines_copies SET name_copy = NEW.name, description_copy = NEW.description, picture_path_copy = NEW.picture_path, path_copy = NEW.path, date_copy = NEW.date where magazine_id = NEW.magazine_id and magazine_copy_id = NEW.id;
            RETURN NEW;
        ELSIF (TG_OP = 'INSERT') THEN
            WITH tem AS (
		SELECT DISTINCT * from magazines where id = NEW.magazine_id
		)
            INSERT INTO magazines_magazines_copies VALUES((select id from tem), (select name from tem), (select description from tem) , (select picture_path from tem), (select path from tem), NEW.id, NEW.name, NEW.description, NEW.picture_path, NEW.path, NEW.date);
            RETURN NEW;
        END IF;
    END;
 $$ LANGUAGE plpgsql;

create trigger update_magazine_denormal before update or insert or delete on "magazines_copies"
	for each row
	execute procedure update_magazine_magazine_copy();
	
