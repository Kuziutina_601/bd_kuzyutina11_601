﻿CREATE INDEX ON friends(user_id);

CREATE INDEX ON quotations(magazine_id);

CREATE INDEX ON magazines_reviews(magazine_id);

CREATE INDEX ON magazines_copies_reviews(magazine_copy_id);

CREATE INDEX ON quotations(magazine_id);

CREATE INDEX ON users(cookie);

CREATE INDEX ON letters(sender_id);

CREATE INDEX ON letters(recepient_id);

CREATE INDEX ON subscriptions(user_id);